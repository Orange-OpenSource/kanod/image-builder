# Kanod Image Builder

CLI and core library to build custom OS image in Kanod.

Documentation: 

* building and designing images: https://orange-opensource.gitlab.io/kanod/reference/image-builder/index.html
* core library: https://orange-opensource.gitlab.io/kanod/reference/machines/core/index.html
