options:
- name: debug
  kind: var
  help: build a debug version of the target OS
- name: tpm
  kind: flag
  help: configure for the TPM mode.
- name: cloud_init_version
  kind: var
  help: version of cloud-init used in the image.
  default: '24.2'
- name: target
  default: ubuntu
  choices:
  - ubuntu
  - centos
  - opensuse
  kind: var
  help: target os distribution.
- name: os_version
  kind: var
  help: name of the version of the OS (usually git version).
- name: os_name
  kind: var
  help: name of the target OS (usually name on repository).
- name: image_size
  kind: var
  help: size of the image.
- name: packages
  kind: var
  help: additional packages.
- name: release
  default: bionic
  kind: var
  help: relase name of the OS. The releas name depends on the target and it should follow disk_image_build expectation.
- name: lvm
  kind: flag
  help: flag controlling the use of LVM partitioning
- name: cis_remediation
  kind: flag
  help: flag controlling elements implementing CIS hardening
- name: rename_interface_names
  kind: flag
  help: flag controlling the renaming of interface names
- name: grub_config
  kind: flag
  help: flag controlling the configuration of grub
- name: image
  kind: var
  help: image used as seed for the image construction
- name: no_kanod_network
  kind: flag
  help: flag to disable kanod networking
env:
- name: DIB_TPM2_TOOLS
  value: "5.2"
- name: DIB_TPM2_TSS
  value: "2.4.6"
- name: DIB_PYVER
  value: "3.8.13"
- name: OVERWRITE_OLD_IMAGE
  value: '1'
- name: DIB_DEV_USER_PWDLESS_SUDO
  value: 'yes'
  when:
  - debug
- name: DIB_DEV_USER_PASSWORD
  value: '{{debug}}'
  when:
  - debug
- name: DIB_DEV_USER_SHELL
  value: /bin/bash
  when:
  - debug
- name: DIB_LOCAL_IMAGE
  value: '{{image}}'
  when:
  - image
  - '!image=-'
- name: DIB_IMAGE_SIZE
  value: '{{image_size}}'
  when:
  - image_size
- name: DIB_CLOUD_INIT_VERSION
  value: '{{cloud_init_version}}'
- name: DIB_CLOUD_INIT_ETC_HOSTS
  value: 'true'
- name: DIB_CLOUD_INIT_DATASOURCES
  value: 'NoCloud'
- name: DIB_YUM_MINIMAL_CREATE_INTERFACES
  value: '0'
- name: DIB_RELEASE
  when:
  - target=ubuntu
  value: '{{release | default("bionic")}}'
- name: DIB_RELEASE
  when:
  - target=centos
  value: '{{release | default("8")}}'
- name: DIB_RELEASE
  when:
  - target=opensuse
  value: '{{release | default("15.4")}}'
- name: DIB_BOOTLOADER_DEFAULT_CMDLINE
  value: 'nofb nomodeset gfxpayload=text net.ifnames=1'
  when:
  - target=centos
- name: DIB_KANOD_ROLE
  value: core
- name: DIB_KANOD_LIBRARIES
  value: ''
- name: DIB_PYTHON3
  value: python3.8
  when:
  - 'target=centos'
  - 'release=7'
- name: DIB_PYTHON3
  value: python3.8
  when:
  - 'target=centos'
  - 'release=8-stream'
- name: DIB_PYTHON3
  value: python3.8
  when:
  - 'target=ubuntu'
  - 'release=bionic'
- name: DIB_PYTHON3
  value: python3.11
  when:
  - 'target=opensuse'
- name: YUM
  value: dnf
  when:
  - 'target=centos'
  - '!release=7'
- name: DIB_BOOTLOADER_DEFAULT_CMDLINE
  value: 'nofb nomodeset gfxpayload=text net.ifnames=1'
  when:
  - target=opensuse
- name: DIB_NO_KANOD_NETWORK
  value: '1'
  when:
  - no_kanod_network
- name: DIB_KANOD_OS_VERSION
  value: '{{os_version}}'
- name: DIB_KANOD_OS_NAME
  value: '{{os_name}}'

recipes:
- elements:
  - kanod-mirror
  - kanod-cloud-init
  - kanod-admin
  - vm
  - openssh-server
  - runtime-ssh-host-keys
  - growroot
  - bootloader
  - grub-init
  - kanod-configure
  - chrony
  - sysctl
- when:
  - tpm
  elements:
  - tpm2tools
- when:
  - grub_config
  elements:
  - grub-config
- when:
  - target=ubuntu
  packages:
  - initramfs-tools
  - parted
  - kbd
  - lvm2
  - netplan.io
  elements:
  - ubuntu{{ "" if image is defined else "-minimal" }}
  - ubuntu-fix
  - klvm
- when:
  - debug
  elements:
  - devuser
- when:
  - target=ubuntu
  - debug
  packages:
  - vim
  - tcpdump
  - nmap
  - iputils-ping
  - python3-setuptools
- when:
  - target=centos
  packages:
  - epel-release
  - openssh-clients
  elements:
  - centos{{ "" if image is defined else "-minimal" }}
  - yum
  - selinux-permissive
- when:
  - target=centos
  - '!release=9-stream'
  packages:
  - network-scripts
- when:
  - target=centos
  - debug
  packages:
  - nmap
  - python3-setuptools
- when:
  - packages
  packages:
  - '{{packages}}'
- when:
  - target=ubuntu
  - '!lvm'
  elements:
  - block-device-efi
- when:
  - target=ubuntu
  - lvm
  elements:
  - block-device-kanod-lvm
- when:
  - target=ubuntu
  - cis_remediation
  elements:
  - cis-remediation
- when:
  - target=ubuntu
  - rename_interface_names
  elements:
  - rename-interface-names
- when:
  - target=centos
  packages:
  - gdisk
  elements:
  - block-device-gpt
- when:
  - target=centos
  - release=7
  elements:
  - python38
  packages:
  - dhclient
- when:
  - target=opensuse
  packages:
  - virtme
  - nginx
  - python311-base
  - python311-devel
  - python311-pip
  - python311-setuptools
  - python311-wheel
  - gcc
  - apparmor-utils
  - conntrack-tools
  - conntrackd
  - bind-utils
  - patch
  - iproute2
  - NetworkManager
  - systemd-network
  elements:
  - opensuse-minimal
  - block-device-gpt
