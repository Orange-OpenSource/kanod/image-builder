import subprocess
from kanod_configure import common


def sysctl_config(arg: common.RunnableParams):
    sysctl_table = arg.conf.get("sysctl", None)
    if sysctl_table is None:
        return
    print('sysctl config')
    with open('/etc/sysctl.d/99-local.conf', 'w', encoding='utf-8') as fd:
        for (key, value) in sysctl_table.items():
            fd.write(f'{key} = {value}\n')
    proc = subprocess.run(['sysctl', '--system'])
    if proc.returncode != 0:
        print('* reloading sysctl config failed.')


common.register('Sysctl configuration', 40, sysctl_config)
