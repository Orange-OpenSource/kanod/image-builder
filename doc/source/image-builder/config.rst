Configuration of an Image Build
===============================

.. jsonschema:: schema_config.yaml
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target: